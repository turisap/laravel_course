<div class="well post">

    <h2><a href="/posts/{{$post->id}}">{{$post->title}}</a></h2>

    @if(count($post->tags))
        @foreach($post->tags as $tag)
            <h3><a href="/posts/tags/{{$tag->name}}">{{$tag->name}}</a></h3>
        @endforeach
    @endif

    <!-- toFormattedDateString() is used to convert timestamp to a readable date -->
    <p class="lead">Made by: {{$post->user->name}} on {{$post->created_at->toFormattedDateString()}}</p>
    <p>{{$post->body}}</p>
</div>

