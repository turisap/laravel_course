@extends('baselayouts.base')

    @section('title')
        All Posts
    @endsection

    @section('content')

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @foreach($posts as $post)
                        @include('posts.article')
                    @endforeach
                </div>
            </div>
        </div>

    @endsection

    @section('footer')
    <script></script>
    @endsection