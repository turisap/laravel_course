@extends('baselayouts.base')

    @section('content')

        <h1>Create POST</h1>
        <div class="row">
            <div class="col-md-12">

                <!-- place for displaing error messages on form validation -->
                @include('baselayouts.errors')

                <form action="/posts" method="post">

                    <!--this field for safety tokens in laravel (Token mismatch Exception) MUST BE INCLUDED IN ALL FORMS -->
                    {{csrf_field()}}

                    <div class="form-group">
                        <label >Title</label>
                        <input type="text" class="form-control" id="email" name="title">
                    </div>
                    <div class="form-group">
                        <label for="pwd">Body</label>
                        <textarea class="form-control" name="body"></textarea>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>

    @endsection

    @section('footer')
    <script></script>
    @endsection