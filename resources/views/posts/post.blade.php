@extends('baselayouts.base')

    @section('content')

        <div class="well">
        <h1>{{$post->title}}</h1>
        <!-- toFormattedDateString() is used to convert timestamp to a readable date -->
        <p class="lead">Made by: on {{$post->created_at->toFormattedDateString()}}</p>
        <p>{{$post->body}}</p>
        </div>

        <ul class="list-group">
            @foreach($post->comments as $comment)
                <li class="list-group-item">
                    <!-- diffForHumans() is used to convert timestamp to a readable date CARBON library -->
                    <strong>{{$comment->created_at->diffForHumans()}}</strong>
                    <p>{{$comment->body}}</p>
                </li>
            @endforeach
        </ul>

        <div class="well">
                @include('baselayouts.errors')
                <form class="form-horizontal" action="/posts/{{$post->id}}/comment" method="POST">

                    {{csrf_field()}} <!--safety token-->

                    <div class="form-group">
                        <textarea class="form-control" name="body" rows="5"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Add Comment</button>
                </form>
        </div>

    @endsection

    @section('footer')
    <script></script>
    @endsection