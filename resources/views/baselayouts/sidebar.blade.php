<div class="panel">
    <div class="panel-heading">
        <h2>Archives</h2>
    </div>
    <div class="panel-body">
        <ul class="list-unstyled">
            @foreach($archives as $archive)
                <li><a href="?month={{$archive['month']}}&year={{$archive['year']}}"><h3>{{$archive['month']}}  ({{$archive['year']}})</h3></a></li>
            @endforeach
        </ul>
        <ul class="list-unstyled" style="margin-top: 30px;">
            @foreach($tags as $tag)
                <!-- use $tag itself due to we select not all columns from db but just name -->
                <li><a href="/posts/tags/{{$tag}}"><h3>{{$tag}}</h3></a></li>
            @endforeach
        </ul>
    </div>
</div>