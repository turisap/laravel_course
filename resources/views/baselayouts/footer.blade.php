
<footer>
    <div class="footer-social">
        <div class="container text-center">
            <ul class="list-inline">
                <li class="list-inline-item social-github">
                    <a href="https://github.com/blackrockdigital"><i class="fa fa-github"></i></a>
                </li>
                <li class="list-inline-item social-twitter">
                    <a href="https://twitter.com/sbootstrap"><i class="fa fa-twitter"></i></a>
                </li>
                <li class="list-inline-item social-facebook">
                    <a href="https://www.facebook.com/StartBootstrap/"><i class="fa fa-facebook"></i></a>
                </li>
                <li class="list-inline-item social-google-plus">
                    <a href="https://plus.google.com/+Startbootstrap"><i class="fa fa-google-plus"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="footer-main">
        <div class="container">
            <a href="/">Start Bootstrap</a> is a project created and maintained by <a href="http://davidmiller.io">David Miller</a> at <a href="http://blackrockdigital.io">Blackrock Digital</a>.
            <br>
            Themes and templates licensed <a href="https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE">MIT</a>, Start Bootstrap website <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.
            <br>
            Based on <a href="http://getbootstrap.com">Bootstrap</a>.
        </div>
    </div>
</footer>

<div class="modal-search modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <form action="get" id="site_search">
                        <div class="input-group">


                            <input class="form-control" type="text" id="search_box" placeholder="Search themes and templates..." autocomplete="off">
                            <span class="input-group-btn">
<button class="btn" type="submit"><i class="icon-magnifier"></i></button>
</span>
                        </div>
                    </form>
                    <ul style="display: block; margin-top: 15px;" id="search_results"></ul>
                </div>
            </div>
        </div>
    </div>
</div>

@yield('footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/lunr.js/1.0.0/lunr.min.js" integrity="sha256-pFs1YPpT5gvlVt91nLsJiVFDFo67ly0bWUp4AM356/k=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="/js/search.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="/js/scripts.js"></script>
</body>
</html>
