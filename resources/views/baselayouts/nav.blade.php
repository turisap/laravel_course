
<nav class="navbar fixed-top navbar-toggleable-md navbar-light sb-navbar">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarExample" aria-controls="navbarExample" aria-expanded="false" aria-label="Toggle navigation">
        Menu <i class="fa fa-bars"></i>
    </button>
    <div class="container">
        <a class="navbar-brand" href="/">Start Bootstrap</a>
        <div class="collapse navbar-collapse" id="navbarExample">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/articles" title="Start Bootstrap News, Guides, and Tutorials">Articles</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/contact" title="Contact Start Bootstrap">Contact</a>
                </li>
            </ul>
            <nav class="navbar-nav navbar-right">
                @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" href="/contact">{{Auth::user()->name}}</a>
                    </li>
                @endif
            <nav>
        </nav>
    </nav>
</nav>
