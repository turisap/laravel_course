
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <script type="text/javascript">
        var host = "startbootstrap.com";
        if ((host == window.location.host) && (window.location.protocol != "https:"))
            window.location.protocol = "https";
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A free Bootstrap blog theme perfect for personal blogs. All Start Bootstrap templates are free to download and open source.">
    <meta name="author" content="Start Bootstrap">
    <meta name="google-site-verification" content="37Tru9bxB3NrqXCt6JT5Vx8wz2AJQ0G4TkC-j8WL3kw">
    <title>
        Clean Blog - Bootstrap Blog Theme - Start Bootstrap
    </title>

    <link rel="author" href="https://plus.google.com/+Startbootstrap">
    <link rel="publisher" href="https://plus.google.com/+Startbootstrap">

    <link rel="canonical" href="https://startbootstrap.com/template-overviews/clean-blog/">

    <link href='https://fonts.googleapis.com/css?family=Libre+Franklin:400,100,100italic,200,200italic,300,300italic,400italic,500,600,500italic,600italic,700,700italic,800,900,800italic,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans+Caption:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Patrick+Hand+SC' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.3.2/css/simple-line-icons.css" type="text/css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

    <link rel="stylesheet" href="/css/boostrap.css ">
    <link rel="stylesheet" href="/css/app.css ">

    <link rel="alternate" type="application/rss+xml" title="" href="https://startbootstrap.com /feed.xml ">

    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/img/favicons/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/favicons/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/favicons/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/favicons/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/img/favicons/apple-touch-icon-60x60.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/img/favicons/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/img/favicons/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/img/favicons/apple-touch-icon-152x152.png"/>
    <link rel="icon" type="image/png" href="/img/favicons/favicon-196x196.png" sizes="196x196"/>
    <link rel="icon" type="image/png" href="/img/favicons/favicon-96x96.png" sizes="96x96"/>
    <link rel="icon" type="image/png" href="/img/favicons/favicon-32x32.png" sizes="32x32"/>
    <link rel="icon" type="image/png" href="/img/favicons/favicon-16x16.png" sizes="16x16"/>
    <link rel="icon" type="image/png" href="/img/favicons/favicon-128.png" sizes="128x128"/>
    <meta name="application-name" content="&nbsp;"/>
    <meta name="msapplication-TileColor" content="#FFFFFF"/>
    <meta name="msapplication-TileImage" content="/img/favicons/mstile-144x144.png"/>
    <meta name="msapplication-square70x70logo" content="/img/favicons/mstile-70x70.png"/>
    <meta name="msapplication-square150x150logo" content="/img/favicons/mstile-150x150.png"/>
    <meta name="msapplication-wide310x150logo" content="/img/favicons/mstile-310x150.png"/>
    <meta name="msapplication-square310x310logo" content="/img/favicons/mstile-310x310.png"/>

    <meta property="og:title" content="Clean Blog - Bootstrap Blog Theme">
    <meta property="og:site_name" content="Start Bootstrap">
    <meta property="og:type" content="website">
    <meta property="og:description" content="A free Bootstrap blog theme perfect for personal blogs. All Start Bootstrap templates are free to download and open source.">
    <meta property="og:image" content="/assets/img/og/startbootstrap-logo.jpg">
    <meta property="og:url" content="https://startbootstrap.com/template-overviews/clean-blog/">
    <style>.navbar-toggler{z-index:1;}@media (max-width: 576px) {nav>.container{width:100%;}}</style>
</head>
<body>

@include('baselayouts.nav')
@if($flash = session('message'))
    <div id="flashMessageCustom" class="alert alert-danger">
        <p>{{$flash}}</p>
    </div>
@endif
<header class="page-heading">
    <div class="container">
        <h1>Clean Blog</h1>
        <p>A clean, Bootstrap blog theme ready to hook into your favorite CMS or blogging platform.</p>
        <script async type="text/javascript" src="//cdn.carbonads.com/carbon.js?zoneid=1673&serve=C6AILKT&placement=startbootstrapcom" id="_carbonads_js"></script>
    </div>
</header>

<div class="container">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/">Start Bootstrap</a></li>
        <li class="breadcrumb-item active">Clean Blog</li>
    </ol>
</div>

<div class="container">
<div class="row">
    <div class="col-md-8">
        @yield('content')
    </div>
    <div class="col-md-4">
        @include('baselayouts.sidebar')
    </div>
</div>

</div>


<script type="text/javascript" src="/js/app.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        setTimeout(fadeMessage, 3000);
    });

    function fadeMessage(){
        $('#flashMessageCustom').fadeOut(500);
    }
</script>
@include('baselayouts.footer')