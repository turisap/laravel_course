@component('mail::message')
# Introduction

Thanks a lot for registering

@component('mail::button', ['url' => 'http://laracast.com'])
Discover
@endcomponent

@component('mail::panel', ['url' => 'http://laracast.com'])
Some staff to get inspired
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
