@extends('baselayouts.base')


@section('content')
    @include('baselayouts.errors')
    <form class="form-horizontal" action='' method="POST">
        {{csrf_field()}}
        <fieldset>
            <div id="legend">
                <legend class="">Register</legend>
            </div>
            <div class="control-group">
                <div class="form-qroup">
                    <label class="control-label" >Name</label>
                    <input type="text" id="username" name="name" placeholder="Your name" class="form-control" required>
                </div>
            </div>
            <div class="control-group">
                <div class="form-qroup">
                    <label class="control-label">Email</label>
                    <input type="email" id="email" name="email" placeholder="Your email" class="form-control" required>
                </div>
            </div>
            <div class="control-group">
                <div class="form-qroup">
                    <label class="control-label">Password</label>
                    <input type="password" id="password" name="password" placeholder="Your @#%kdjh30jdf9045" class="form-control" required>
                </div>
            </div>
            <div class="control-group">
                <div class="form-qroup">
                    <label class="control-label">Password Confirmation</label>
                    <!-- pay attention how is named confirmation field-->
                    <input type="password" name="password_confirmation" placeholder="Your @#%kdjh30jdf9045" class="form-control" required>
                </div>
            </div>
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit">Register</button>
                </div>
            </div>
        </fieldset>
    </form>


@endsection