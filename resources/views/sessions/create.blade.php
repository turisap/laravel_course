@extends('baselayouts.base')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Log in</h1>
            @include('baselayouts.errors')
            <form action="/login" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" name="email" type="email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input class="form-control" name="password" type="password">
                </div>
                <button class="btn btn-default" type="submit">Login</button>
            </form>
        </div>
    </div>
@endsection