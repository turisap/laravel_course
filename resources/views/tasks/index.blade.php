<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
</head>
<body>
@foreach($tasks as $task)
<h1><a href="/tasks/{{$task->id}}">{{$task->body}}</a></h1>
    @endforeach
</body>
</html>