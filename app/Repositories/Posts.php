<?php

namespace App\Repositories;

use App\Post;
use App\Redis;

class Posts
{

     protected $redis;

     // this is an example on nested dependencies: instantiating of this class invokes instantiation on Redis
     public function __construct(Redis $redis)
     {

        $this->redis = $redis;

     }

     public function all()
     {

         return Post::all();

     }

}