<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeAgain;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserSaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|min:2',
            'email'    => 'required|email',
            'password' => 'required|confirmed' // pay attention how respective field is named in the form
        ];
    }


    // this method is used for extracting all registration staff from the respective controller
    public function persist()
    {

        // create and save the user
        $user = User::create([
            'name'     => request('name'),
            'email'    => request('email'),
            'password' => Hash::make(request('password'))
        ]);

        // log user in
        auth()->login($user);

        //mail user
        Mail::to($user)->send(new WelcomeAgain($user));

    }
}
