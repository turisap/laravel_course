<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogPostRequest;
use App\Post;
use App\Repositories\Posts;



class PostsController extends Controller
{

    // action filter
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']); // action filter which allows only authentificated users with a few exceptions
    }


    // show all posts
    public function index(Posts $posts)
    {
        //dd($posts); // output posts class with nested Redis class

        //find all posts with filters from query string
        $posts = Post::latest()->filter(request(['year', 'month']))->get();

        return view('posts.posts', compact('posts'));

    }



    //find post using wrap model binding (variable should be the same as wildcharacter from the route {post})
    public function show(Post $post)
    {

        return view('posts.post', compact('post'));

    }



    // displays form
    public function create()
    {
        return view('posts.create');
    }



    // processes POST from form (arguments are for form validation)
    public function store(StoreBlogPostRequest $request)
    {
        //dd(request()->all()); // vardump all names or just particular one
        //dd(request('title'));

       /* //create new post
        $post = new Post();

        //assign values from form
        $post->title = request('title');
        $post->body  = request('body');

        $post->save();*/

       // form validation  MOVED TO StoreBlogPostRequest.php
        /*$this->validate(request(), [
            // fields and rules
            'title' => 'required|min:8',
            'body'  => 'required'
        ]);*/


        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );

        // add flash message
        session()->flash('message', 'Your post has been published');


        return redirect('/');

    }

}
