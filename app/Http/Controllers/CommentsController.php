<?php

namespace App\Http\Controllers;


use App\Http\Requests\StoreCommentRequest;
use App\Post;
use App\Comment;

class CommentsController extends Controller
{

    public function store(Post $post)
    {


        // create a comment          // moved to addComment()
        /*Comment::create([
            'body'    => request('body'),
            'post_id' => $post->id
        ]);*/
        $this->validate(request(), ['body' => 'required|min:8']);
        $post->addComment(request('body')); // request('body') as an argument

        //redirect back to the original page
        return back();
    }

}
