<?php

namespace App\Http\Controllers;

use App\Task;

class TasksController extends Controller
{
    public function index(){
        $tasks = Task::all();
        return view('tasks.index', compact('tasks'));
    }

   /* public function show($id){                                          //finding a task using simple argument
        $task = Task::find($id);
        return $task;
        return view('tasks.show', compact('task'));
    }*/

    public function show(Task $task)                                      // finding an item using model binding (model and argument are passed as params)
    {

        //return $task;
        return view('tasks.show', compact('task'));

    }
}
