<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserSaveRequest;
use App\Mail\WelcomeAgain;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\Welcome;

class RegistrationController extends Controller
{



    public function create()
    {
        return view('registration.create');
    }

    public function store(UserSaveRequest $request)
    {
        // run all the code which was moved to the Request class
        $request->persist();

        // add flash message
        session()->flash('message', 'Thanks for signing in!');

        // redirect to the homepage
        return redirect()->home();

    }


}
