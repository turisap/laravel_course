<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{

    // action filter
    public function __construct()
    {

        $this->middleware('guest')->except(['destroy', 'store']);

    }

    // shows login page
    public function create()
    {
        return view('sessions.create');
    }


    // log user out
    public function destroy()
    {
        auth()->logout();
        return redirect()->home();
    }

    // logins user
    public function store()
    {

        if(! auth()->attempt(request(['email', 'password']))){
            return back()->withErrors([
                'message' => 'Please check your credentials'
            ]);
        }

        return redirect()->home();

    }
}
