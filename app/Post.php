<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use PhpParser\Builder\Class_;
use Carbon\Carbon;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'user_id']; // this property prevents appearing mass assigning exception while submiting for specified fields
    protected $guarded  = ['user_id']; // this array consist of names in a form which a prohibited to submit



    // returns all comments to one post
    public function comments()
    {

        return $this->hasMany(Comment::class);

    }

    // returns a particular user which particular post belongs to
    public function  user()  // $post->user
    {

        return $this->belongsTo(User::class);

    }

    public function addComment($body)
    {

        $this->comments()->create(compact('body')); // this is instead on code beneath ( post_id passed automatically as part of object instance)
        /*Comment::create([
            'body'    => $body,
            'post_id' => $this->id
        ]);*/

    }


    // filters posts by month and year
    public function scopeFilter($query, $filter)
    {

        // add where clause if we have GET request
        if($month = $filter['month']){
            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }
        if($year = $filter['year']){
            $query->whereYear('created_at', $year);
        }

    }


    // returns arhcives for the sidebar
    public static function archives()
    {

        return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
            ->groupBy('year', 'month')->orderByRaw('min(created_at) desc')->get()->toArray();


    }


    // this method establishes relationships between posts and tags
    // this is many-to-many relationships
    public function tags()
    {

        return $this->belongsToMany(Tag::class);

    }




}


