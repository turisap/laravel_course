<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 03-Jul-17
 * Time: 9:08 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Model extends Eloquent     // this core class extends eloquent and consist all guarder and fillable for all models
{
    protected $fillable = ['title', 'body']; // this property prevents appearing mass assigning exception while submiting for specified fields
    protected $guarded  = ['user_id']; // this array consist of names in a form which a prohibited to submit
}