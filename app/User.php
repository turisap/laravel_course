<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',  // if create a user via cmd, comment out password
    ];




    // returns all posts of one user    ESTABLISH RELATIONSHIPS BETWEEN POST AND USER MODELS
    public function posts()
    {

       return  $this->hasMany(Post::class);

    }


    //publish post
    public function publish(Post $post)
    {

        $this->posts()->save($post);

    }
}

