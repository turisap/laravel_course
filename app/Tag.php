<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;

class Tag extends Model
{

    // this establish one-to-many relationships
    // one tag to many posts and one post to many tags (the same in Post model)
    public function posts()
    {

        return $this->belongsToMany(Post::class);

    }


    // THIS METHOD ALLOWS TO GET NOT ID FROM ROUTE BUT NAME AND FINDS A RECORD ACCORDINGLY
    public function getRouteKeyName()
    {
        return 'name';
    }
}
