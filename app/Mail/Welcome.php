<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;   // you must to specify user as public in order to get it available throung all views

    // we pass the new registered user to constructor in order to get it available in the view
    public function __construct(User $user)
    {
        $this->user = $user; // user object becomes available in all views of this class
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    // here we need to specify a view which will be sent as email
    public function build()
    {
        return $this->view('emails.welcome');
    }
}
