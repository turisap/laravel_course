<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable = ['body', 'post_id']; // this property prevents appearing mass assigning exception while submiting for specified fields


    public function  post()
    {
        // returns a post which a particular comment belongs  to
        return $this->belongsTo(Post::class);

    }

    // returns a particular user which particular comment belongs to
    public function  user()  // $comment->user
    {

        return $this->belongsTo(User::class);

    }


}
