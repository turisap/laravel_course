<?php

namespace App\Providers;

use App\Stripe;
use App\Tag;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Post;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    // this is kind of twig global
    // we use it here to register results of archives() method to all views which load sidebar
    public function boot()
    {
        view()->composer('baselayouts.sidebar', function($view){
            //archives for sidebar
            // tags for the sidebar
            $archives = Post::archives();
            $tags     = Tag::has('posts')->pluck('name');  // gives only those ones which have posts
            $view->with(compact('archives', 'tags'));

        });

        // fix for the bug of migration Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    // register stm for service container. In this case we register instance of Stripe class with passing stripe secret key from config file
    // Now any instance everywhere will contain this key ( kind of twig global but for classes) config() passes a var from config file to the class's constructor
    public function register()
    {
        $this->app->singleton(Stripe::class, function (){
            return new Stripe(config('services.stripe.secret'));
        });
    }
}
