<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */

    // here we must register our events and their listeners BEFORE run php artisan event:generate
    protected $listen = [
        'App\Events\ThreadCreated' => [
            'App\Listeners\NotifySubscriber',
            'App\Listeners\CheckForSpam'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
