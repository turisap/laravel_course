<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Tarampam;

class SocialMediaProvider extends ServiceProvider
{


    // this class is crated manually and used for registraion, f.e. instances of classes with social media APIs keys
    // registering it globally
    // after populating tyhis class you need to register it in config/app.php
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Tarampam::class, function (){
            return new Tarampam(config('myconfig.tarampam'));
        });
    }
}
