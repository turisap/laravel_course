<?php

//use App\Task;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/tasks/{task}', function ($id) {        //getting task using DB class
    $task = DB::table('tasks')->find($id);
    //dd($tasks);
    // $tasks;
    return view('tasks.show', compact('task'));
});*/

/*Route::get('/tasks', function () {               //getting tasks using query builder
    $tasks = DB::table('tasks')->get();
    //dd($tasks);
    // $tasks;
    return view('tasks.index', compact('tasks'));
});*/


/*Route::get('/tasks', function () {                   // getting tasks using Eloquent
    $tasks = Task::all();
    return view('tasks.index', compact('tasks'));
});

Route::get('/tasks/{task}', function ($id) {        // gettinh task using Eloquent
    $task = Task::find($id);
    return view('tasks.show', compact('task'));
});*/

/*Route::get('/tasks', 'TasksController@index');                   // using task controller for routing
Route::get('/tasks/{task}', 'TasksController@show');*/                   // using task controller for routing



Route::get('/test', 'TestsController@index');                         // route for testing purposes

Route::get('/', 'PostsController@index')->name('home');
Route::get('/posts/tags/{tag}', 'TagsController@index');              //route for posts and tags

Route::get('/posts/create', 'PostsController@create');
Route::get('/posts/{post}', 'PostsController@show');
Route::post('/posts', 'PostsController@store');                       // this route for processing POSTrequest from form create post
Route::post('/posts/{post}/comment', 'CommentsController@store');     // this route for processing POSTrequest from form create comment

Route::get('/login', 'SessionsController@create')->name('login');                     // route for login in page showing
Route::post('/login', 'SessionsController@store');                     // route for login in form processing
Route::get('/register', 'RegistrationController@create');             // route for registration - get request shows the form
Route::post('/register', 'RegistrationController@store');             // route for processing of the registration form post request process the submission
Route::get('/logout', 'SessionsController@destroy');                  // logout
